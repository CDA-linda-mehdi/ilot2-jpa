package com.afpa.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.afpa.dao.EnergieDao;
import com.afpa.entity.Couleur;
import com.afpa.entity.Energie;
import com.afpa.entity.Modele;
import com.afpa.entity.Voiture;

@TestMethodOrder(value = OrderAnnotation.class)
public class EnergieDaoTest {

	static EnergieDao dao;

	@BeforeAll
	public static void beforeAll() {
		dao = new EnergieDao();

	}

	@AfterAll
	public static void afterAll() {
		dao.close();
	}

	private static Energie energie;

	@Test
	@Order(1)
	public void ajoutEnergie() {
		Energie e = Energie.builder().label("diesel").build();
		e = dao.add(e);
		assertNotNull(e);
		assertNotEquals(0, e.getCode());
		energie = e;
	}

	@Test
	@Order(2)
	public void miseAjourEnergie() {
		energie.setLabel("élèctrique");
		dao.update(energie);
		Energie e = dao.find(energie.getCode());
		assertNotNull(e);
		assertEquals("élèctrique", energie.getLabel());
	}

	@Test
	@Order(3)
	public void supprimerEnergie() {
		dao.remove(energie.getCode());
		Energie e = dao.find(energie.getCode());
		assertNull(e);
	}

	@Test
	@Order(4)
	public void ajoutModeleAvecEnergie() {
		Energie e = Energie.builder().label("Essence").build();

		Modele m = new Modele();
		m.setEnergie(e);

		Modele m2 = new Modele();
		m2.setEnergie(e);

		List<Modele> modeles = Arrays.asList(m, m2);
		e.setModeles(new HashSet<Modele>(modeles));

		energie = dao.add(e);
		assertNotNull(energie);
		assertNotEquals(0, energie.getCode());
	}

	@Test
	@Order(5)
	public void listerEnergie() {
		Collection<Energie> energies = dao.findAllNamedQuery("listEnergie");
		assertNotEquals(0, energies.size());
		energies.stream().forEach(System.out::println);

	}
	
	@Test
	@Order(6)
	public void trouverEnergie() {
		Energie e = dao.find(energie.getCode());
		assertNotNull(e);
		assertEquals(energie.getCode(), e.getCode());
	}

}
