package com.afpa.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.afpa.dao.MarqueDao;
import com.afpa.entity.Couleur;
import com.afpa.entity.Marque;
import com.afpa.entity.Modele;
import com.afpa.entity.Voiture;

@TestMethodOrder(value = OrderAnnotation.class)

public class MarqueDaoTest {

	static MarqueDao dao;

	@BeforeAll
	public static void beforeAll() {
		dao = new MarqueDao();

	}

	@AfterAll
	public static void afterAll() {
		dao.close();
	}

	private static Marque marque;

	@Test
	@Order(1)
	public void ajoutMarque() {
		Marque m = Marque.builder().label("Renault").build();
		m = dao.add(m);
		assertNotNull(m);
		assertNotEquals(0, m.getCode());
		marque = m;
	}

	@Test
	@Order(2)
	public void miseAjourMarque() {
		marque.setLabel("Mercedes");
		dao.update(marque);
		Marque m = dao.find(marque.getCode());
		assertNotNull(m);
		assertEquals("Mercedes", marque.getLabel());
	}

	@Test
	@Order(3)
	public void supprimerMarque() {
		dao.remove(marque.getCode());
		Marque m = dao.find(marque.getCode());
		assertNull(m);
	}

	@Test
	@Order(4)
	public void ajoutModeleMArque() {
		Marque m = Marque.builder().label("Peugeot").build();

		Modele modele = Modele.builder().label("306").build();
		modele.setMarque(m);

		Modele modele2 = Modele.builder().label("206").build();
		modele2.setMarque(m);

		List<Modele> modeles = Arrays.asList(modele, modele2);
		m.setModeles(new HashSet<Modele>(modeles));

		marque = dao.add(m);
		assertNotNull(marque);
		assertNotEquals(0, marque.getCode());
	}


	@Test
	@Order(6)
	public void listerMarque() {
		Collection<Marque> marques = dao.findAllNamedQuery("listeMarque");
		assertNotEquals(0, marques.size());
		marques.stream().forEach(System.out::println);

	}
	
	@Test
	@Order(7)
	public void trouverMarque() {
		Marque m = dao.find(marque.getCode());
		assertNotNull(m);
		assertEquals(marque.getCode(), m.getCode());
	}

}
