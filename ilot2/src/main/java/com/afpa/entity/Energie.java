package com.afpa.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(of= {"code","label"})
@Table(name = "t_energie")

@NamedQueries({ @NamedQuery(name = "listEnergie", query = "SELECT e FROM Energie e") })

/**
 * <b>Energie est la classe représentant un type d'énergie d'une Voiture.</b>
 * <p>
 * Une entité Energie est caractérisé par les informations suivantes :
 * <ul>
 * <li>Un code unique attribué automatiquement et définitivement</li>
 * <li>Un label indiquant le nom de l'Energie</li>
 * </p>
 * <p>
 * Une Energie a une liste de voiture.Il sera possible d'ajouter ou supprimer des voitures à cette liste.
 * </p>
 * 
 * @see Voiture
 * 
 * @author Amaia, Badrane, Linda
 * @version 1.0 
 */
public class Energie {
	
	/**
	 * Le code de l'Energie. Ce code n'est pas modifiable.
	 * Il représente la colonne t_code dans la table t_energie
	 * Il est la clé primaire.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int code;
	
	/**
	 * Le label de l'energie. Ce label est changeable.
	 * Il représente la colonne t_label de la table t_energie.
	 */
	private String label;

	/**
	 * La liste des modèles d'une Energie.
	 * Il est possible d'ajouter ou de supprimer des voitures de cette liste.
	 */
	@OneToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST }, fetch = FetchType.LAZY, mappedBy = "energie")
	private Set<Modele> modeles;
}
