package com.afpa.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(of= {"code","label"})
@Table(name = "t_marque")

@NamedQueries({ @NamedQuery(name = "listeMarque", query = "SELECT m FROM Marque m") })

/**
 * <b>Marque est la classe représentant une marque de voiture</b>
 * <p>
 * Une marque est caractérisée par les informations suivantes :
 * <ul>
 * <li>Un code unique attribué automatiqument et définitivement</li>
 * <li>Un label indiquant le nom de la Marque</li>
 * </ul>
 * </p>
 * <p>
 * De plus une MArque a une liste de Modele. Il sera possible d'ajouter 
 * ou supprimer des Modele à cette liste.
 * </p>
 * 
 * @see MArque
 * 
 * @author Amaia, Badrane, Linda
 * @version 1.0
 */
public class Marque {
	
	/**
	 * Le code de la Marque. Ce code n'est pas modifiable
	 * Il représente la colonne t_code de la table t_marque.
	 * Et a la propriété de clé primaire.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int code;
	
	/**
	 * Le label de la Marque. Ce label est changeable.
	 * Il représente la colonne t_label de la table t_marque.
	 */
	private String label;

	/**
	 * La liste des modèles d'une Marque.
	 * Il est possible d'ajouter ou de supprimer des modèles de cette liste.
	 * 
	 * @see Modele
	 */
	@OneToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST }, fetch = FetchType.LAZY, mappedBy = "marque")
	private Set<Modele> modeles;
}
