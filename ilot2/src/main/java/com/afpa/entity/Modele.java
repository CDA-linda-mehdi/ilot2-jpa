package com.afpa.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(of= {"code","label","puissance"})
@Builder
@Table(name = "t_modele")
@NamedQueries({ @NamedQuery(name = "listeModele", query = "SELECT m FROM Modele m") })

/**
 * <b>Modele est la classe représentant un modèle de voiture</b>
 * <p>
 * Un modèle est caractérisée par les informations suivantes :
 * <ul>
 * <li>Un code unique attribué automatiqument et définitivement</li>
 * <li>Un label indiquant le nom de la Marque</li>
 * <li>Une puissance indiquant le nombre de chevaux de la voiture</li>
 * <li>Un type d'Energie</li>
 * <li>La Marque à laquelle il appartient</li>
 * </ul>
 * </p>
 * 
 * @see Marque
 * 
 * @author Amaia, Badrane, Linda
 * @version 1.0
 */
public class Modele {
	
	/**
	 * Le code du Modele. Ce code n'est pas modifiable
	 * Il représente la colonne t_code de la table t_modele.
	 * Et a la propriété de clé primaire.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int code;
	
	/**
	 * Le label du Modele. Ce label est changeable.
	 * Il représente la colonne t_label de la table t_modele.
	 */
	private String label;
	
	/**
	 * La puissance du Modele. Ce label est changeable.
	 * Il représente la colonne t_puissance de la table t_modele.
	 */
	private int puissance;

	/**
	 * L'Energie du Modele. Ce label est changeable.
	 * Il représente la colonne energie de la table t_modele.
	 */
	@ManyToOne
	@JoinColumn(name = "energie")
	private Energie energie;

	/**
	 * La Marque du Modele. Ce label est inchangeable.
	 * Il représente la colonne t_marque de la table t_modele.
	 * Et a la propriété de clé étrangère.
	 */
	@ManyToOne
	@JoinColumn(name = "marque")
	private Marque marque;

	@OneToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST }, fetch = FetchType.LAZY, mappedBy = "modele")
	private Set<Voiture> voitures;
}
