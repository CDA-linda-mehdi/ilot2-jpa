package com.afpa.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Table(name = "t_voiture")
@NamedQueries({
	@NamedQuery(name = "listeVoiture", query = "SELECT v FROM Voiture v"),
	@NamedQuery(name = "listVoitureByCouleurJpql", query = "select v from Voiture v where v.couleur.code = :codeCouleur"),
	@NamedQuery(name = "listeVoitureBestPuissanceByMarque", query = "select v from Voiture v inner join Modele mo ON v.modele.code = mo.code WHERE puissance = (SELECT MAX(puissance) FROM Voiture v INNER JOIN Modele mo ON v.modele.code = mo.code)")
})
	//  @NamedNativeQuery(name = "listVoitureByCouleur", query = "select * from t_voiture where couleur = 116")
	//	@NamedQuery(name = "listeVoitureBestPuissanceByMarque", query = "SELECT v.* FROM ((t_voiture as v LEFT JOIN t_marque ma ON v.modele = ma.modeles) LEFT JOIN t_energie as e ON ") })

/**
 * <b>Voiture est la classe représentant une voiture</b>
 * <p>
 * Une Voiture est caractérisée par les informations suivantes :
 * <ul>
 * <li>Un ID unique attribué automatiquement</li>
 * <li>Un numéro d'imatriculation</li>
 * <li>Un modèle de Voiture</li>
 * <li>Une couleur de voiture</li>
 * </ul>
 * </p>
 * 
 * @see Modele
 * @see Voiture
 * @see Couleur
 * 
 * @author Amaia, Badrane, Linda
 * @version 1.0
 */
public class Voiture {
	
	/**
	 * L'ID. Ce code n'est pas modifiable
	 * Il représente la colonne id de la table t_voiture.
	 * Et a la propriété de clé primaire.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	/**
	 * Le code d'immatriculation. Ce code est modifiable.
	 * Il représente la colonne matricule de la table t_voiture.
	 */
	private String matricule;

	/**
	 * Le Modele de la Voiture. Ce Modele est changeable.
	 * Il représente la colonne modele de la table t_voiture.
	 */
	@ManyToOne
	@JoinColumn(name = "modele")
	private Modele modele;

	/**
	 * La Couleur de la Voiture. Cette Couleur est changeable depuis son Setter.
	 * Elle représente la colonne coulor de la table t_voiture.
	 */
	@ManyToOne
	@JoinColumn(name = "couleur")
	private Couleur couleur;

}