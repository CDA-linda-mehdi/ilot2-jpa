package com.afpa.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(of= {"code","label"})
@Table(name = "t_couleur")
@NamedQueries({ @NamedQuery(name = "listCouleur", query = "SELECT c FROM Couleur c") })

/**
 * <b>Couleur est la classe représentant une couleur de voiture</b>
 * <p>
 * Une Couleur est caractérisée par les informations suivantes :
 * <ul>
 * <li>Un code unique attribué automatiqument et définitivement</li>
 * <li>Un label indiquant le nom de la Couleur</li>
 * </ul>
 * </p>
 * <p>
 * De plus une Couleur a une liste de Voiture. Il sera possible d'ajouter 
 * ou supprimer des voitures à cette liste.
 * </p>
 * 
 * @see Voiture
 * 
 * @author Amaia, Badrane, Linda
 * @version 1.0
 */
public class Couleur {
	
	/**
	 * Le code de la Couleur. Ce code n'est pas modifiable
	 * Il représente la colonne code de la table t_couleur.
	 * Et a la propriété de clé primaire.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int code;

	/**
	 * Le label de la Couleur. Ce label est changeable.
	 * Il représente la colonne t_label de la table t_couleur.
	 */
	private String label;

	/**
	 * La liste de Voiture d'une Couleur.
	 * Il est possible d'ajouter ou de supprimer des voitures de cette liste.
	 * 
	 */
	@OneToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST }, fetch = FetchType.LAZY, mappedBy = "couleur")
	private Set<Voiture> voitures;

}
