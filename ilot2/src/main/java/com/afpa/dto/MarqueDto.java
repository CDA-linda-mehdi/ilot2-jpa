package com.afpa.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class MarqueDto {

	private String label;
}
