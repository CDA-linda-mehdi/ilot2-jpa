package com.afpa.dto;

import com.afpa.entity.Couleur;
import com.afpa.entity.Modele;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class VoitureDto {

	private String matricule;
	private Modele modele;
	private Couleur couleur;

}
