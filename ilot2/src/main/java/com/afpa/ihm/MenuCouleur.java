package com.afpa.ihm;

import java.util.Scanner;

import com.afpa.service.CouleurService;

public class MenuCouleur {

	public static CouleurService ms = new CouleurService();
	
	
	public static void menu() {

		boolean vrai = true;

		while(vrai) {
			System.out.println("");
			System.out.println("------------ Menu Couleur------------");
			System.out.println("");
			System.out.println("1: aouter une couleur");
			System.out.println("2: afficher tout les Couleur");
			System.out.println("3: afficher une couleur par son ID");
			System.out.println("4: metre a jour une couleur");
			System.out.println("5: supprimer une couleur");
			System.out.println("6: fermer menu des couleurs");
			
			System.out.println("entrer votre choix");
			Scanner sc = new Scanner(System.in);
			String choix = sc.next();

			switch (choix) {
			case "1":
				System.out.println("aouter une couleur");
				System.out.println("entré un label pour la couleur");
				String label = sc.next();
				 ms.ajout(label);
				
				break;

			case "2":
				System.out.println("afficher toute les couleurs");
				ms.afficheAll().stream().forEach(System.out::println);;
				break;

			case "3":
				System.out.println("afficher une couleur par son ID");
				System.out.println("entré l'id de la couleur a afficher");
				int id = sc.nextInt();
				System.out.println(ms.afficheID(id));
				break;

			case "4":
				System.out.println("metre a jour une couleur");
				System.out.println("entrer l'id de la couleur a mettre a jour");
				int id1 = sc.nextInt();
				System.out.println("entrer la nouvel couleur string labele");
				String label1 = sc.next();
				ms.update(label1, id1);
				break;
				
			case "5":
				System.out.println("Supprimer une couleur");
				System.out.println("entrer l'ID de la couleur a supprimer");
				int id2 = sc.nextInt();
				ms.remove(id2);
				break;
				
			case "6":
				System.out.println("fin du programme");
				vrai = false;
				break;

			default:
				System.out.println("le choix entrer n'existe pas");
				break;
			}
		}
	}

}
