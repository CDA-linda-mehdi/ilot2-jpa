package com.afpa.ihm;

import java.util.Scanner;
import com.afpa.service.MarqueService;

public class MenuMarque {

	public static MarqueService ms = new MarqueService();
	
	
	public static void menu() {

		boolean vrai = true;

		while(vrai) {
			System.out.println("");
			System.out.println("------------ Menu Marque------------");
			System.out.println("");
			System.out.println("1: ajouter une Marque");
			System.out.println("2: afficher tout les Marque");
			System.out.println("3: afficher une marque par son ID");
			System.out.println("4: metre a jour une marque");
			System.out.println("5: supprimer une marque");
			System.out.println("6: fermer menu des marque");
			
			System.out.println("entrer votre choix");
			Scanner sc = new Scanner(System.in);
			String choix = sc.next();

			switch (choix) {
			case "1":
				System.out.println("aouter une marque");
				System.out.println("entré un label pour la marque");
				String label = sc.next();
				 ms.ajouter(label);
				
				break;

			case "2":
				System.out.println("afficher toute les marques");
				ms.afficherAll().stream().forEach(System.out::println);;
				break;

			case "3":
				System.out.println("afficher une marque par son ID");
				System.out.println("entré l'id de la marque a afficher");
				int id = sc.nextInt();
				System.out.println(ms.findId(id));
				break;

			case "4":
				System.out.println("metre a jour une marque");
				System.out.println("entrer l'id de la marque a mettre a jour");
				int id1 = sc.nextInt();
				System.out.println("entrer la nouvel marque string labele");
				String label1 = sc.next();
				ms.update(label1, id1);
				break;
				
			case "5":
				System.out.println("Supprimer une marque");
				System.out.println("entrer l'ID de la marque a supprimer");
				int id2 = sc.nextInt();
				ms.remove(id2);
				break;
				
			case "6":
				System.out.println("fin du programme");
				vrai = false;
				break;

			default:
				System.out.println("le choix entrer n'existe pas");
				break;
			}
		}
	}

}
