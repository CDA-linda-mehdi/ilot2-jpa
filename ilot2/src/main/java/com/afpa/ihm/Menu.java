package com.afpa.ihm;

import java.util.Scanner;

public class Menu {

	public static void main(String[] args) {

		boolean vrai = true;
		
		while(vrai) {
			System.out.println("");
			System.out.println("---------- menu principale----------");
			System.out.println("");
			System.out.println("1: entrer dans le menu Modele");
			System.out.println("2: entrer dans le menu Couleur");
			System.out.println("3: entrer dans le menu Marque");
			System.out.println("4: entrer dans le menu Voiture");
			System.out.println("5: fin");
			System.out.println("entrer votre choix");
			Scanner sc = new Scanner(System.in);
			String choix = sc.next();
			
			switch (choix) {
			case "1":
				MenuModele.menu();
				break;
				
			case "2":
				MenuCouleur.menu();
				break;
				
			case "3":
				MenuMarque.menu();
				break;
				
			case "4":
				MenuVoiture.menu();
				break;
				
			case "5":
				System.out.println("fin");
				vrai = false;
				break;

			default:
				break;
			}
			
		}

	}



}
