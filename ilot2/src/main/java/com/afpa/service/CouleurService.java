package com.afpa.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.afpa.dao.CouleurDao;
import com.afpa.dto.CouleurDto;
import com.afpa.entity.Couleur;

public class CouleurService {

	CouleurDao sd = new CouleurDao();
	CouleurDto couleurDto = new CouleurDto();

	public void ajout(String couleur) {
		Couleur c = new Couleur();
		c.setLabel(couleur);
		sd.add(c);
	}

	public Collection<CouleurDto> afficheAll() {
		List<CouleurDto> listcouleurdto = new ArrayList<>();
		for (Couleur couleur : sd.findAllNamedQuery("listCouleur")) {
			couleurDto.setLabel(couleur.getLabel());
			listcouleurdto.add(couleurDto);
		}
		return listcouleurdto;
	}

	public CouleurDto afficheID(int id) {
		couleurDto.setLabel(sd.find(id).getLabel());
		return couleurDto;
	}

	public void update(String couleur, int id) {
		Couleur c = sd.find(id);
		c.setLabel(couleur);
		sd.update(c);
	}

	public void remove(int id) {
		sd.remove(id);
	}

}
