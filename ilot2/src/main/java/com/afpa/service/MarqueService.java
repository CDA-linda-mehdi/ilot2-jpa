package com.afpa.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.afpa.dao.MarqueDao;
import com.afpa.dto.EnergieDto;
import com.afpa.dto.MarqueDto;
import com.afpa.entity.Marque;

public class MarqueService {

	MarqueDao md = new MarqueDao();
	MarqueDto marqueDto = new MarqueDto();

	public void ajouter(String label) {
		Marque m = new Marque();
		m.setLabel(label);
		md.add(m);
	}

	public MarqueDto findId(int id) {
		marqueDto.setLabel(md.find(id).getLabel());
		return marqueDto;
	}

	public Collection<MarqueDto> afficherAll() {
		List<MarqueDto> listmarquedto = new ArrayList<>();
		for (Marque marque : md.findAllNamedQuery("listeMarque")) {
			marqueDto.setLabel(marque.getLabel());
			listmarquedto.add(marqueDto);
		}
		return listmarquedto;
	}

	public void update(String label, int id) {
		Marque m = md.find(id);
		m.setLabel(label);
		md.update(m);
	}

	public void remove(int id) {
		md.remove(id);
	}

}
