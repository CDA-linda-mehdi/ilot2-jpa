package com.afpa.dao;

import com.afpa.entity.Couleur;

/**
 * <b>CouleurDao est la classe qui permet la création d'objet Couleur</b>
 * <p>
 * 
 * @author Amaia, Badrane, Linda
 * 
 * @version 1.0
 */
public class CouleurDao extends AbstractDao<Couleur> {

}
