package com.afpa.dao;

import java.util.Collection;

/**
 * <b>IDao est une interface contenant la déclaration de quatre méthodes</b>
 * <p>
 * Les classes implémentant IDao devront obligatoirement implémenter ses méthodes
 * </p>
 * Les méthodes à implémenter sont les suivantes :
 * <ul>
 * <li>Ajouter une entité</li>
 * <li>Trouver un objet par son ID</li>
 * <li>Mettre à jour une enitité</li>
 * <li>Trouver une liste d'objets</li>
 * <li>Supprimer une entité par son ID</li>
 * 
 * @author Amaia, Badrane,Linda
 *
 * @param <T>
 * 				Générécité de l'interface
 * @version 1.0
 */
public interface IDao<T> {

	/**
	 * Ajouter d'une entité
	 * 
	 * @param entity
	 * 			Une entité générée
	 * @return
	 * 			Un objet générique
	 */
	public T add(T entity);
	
	/**
	 * Trouver un objet par son ID
	 * 
	 * @param id
	 * 			L'id de l'objet
	 * @return
	 * 			Un objet générique
	 */
	public T find(int id);
	
	/**
	 * Mettre à jour une enitité
	 * 
	 * @param entity
	 * 			Une entité générée
	 * @return
	 * 			Un objet générique
	 */
	public T update(T entity);
	
	/**
	 * Trouver une liste d'objets
	 * 
	 * @param query
	 * 			Requête permettant de récupérer la liste d'entité
	 * @return La liste de l'entité
	 */
	public Collection<T> findAllNamedQuery(String query);
	
	/**
	 * Supprimer une entité par son ID
	 * 
	 * @param id
	 * 			ID de l'entité
	 */
	public void remove(int id);
}
