package com.afpa.dao;

import java.lang.reflect.ParameterizedType;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <b>AbstractDao est une classe implémentant l'inteface IDao. 
 * Et permet de se connecter à la base de données</b>
 * <p>
 * Toutes les classe héritant (extend) d'AbstractDao pouront utiliser les méthodes suivantes :
 * <ul>
 * <li>Ajouter une entité</li>
 * <li>Trouver une entité par son ID</id>
 * <li>Récupérer une liste d'objets</li>
 * <li>Mettre à jour une entité</li>
 * <li>Supprimer une entité par son ID</li>
 * </ul>
 * </p>
 * @author Amaia, Badrane, Linda
 *
 * @param <T>
 * 				La classe AbstractDao est générique
 * 
 * @see AbstractDao#add(Object)
 * @see AbstractDao#find(Object)
 * @see AbstractDao#
 * 
 * @version 1.0
 */
public class AbstractDao<T> implements IDao<T> {
	private static Logger monLogger = LoggerFactory.getLogger(AbstractDao.class);
	private EntityManagerFactory factory = null;
	private final Class<T> clazz;

	@SuppressWarnings("unchecked")
	public AbstractDao() {
		this.clazz = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
		this.factory = Persistence.createEntityManagerFactory("myBase");
	}

	public void close() {
		if (this.factory != null) {
			this.factory.close();
		}
	}

	public EntityManager newEntityManager() {
		EntityManager em = this.factory.createEntityManager();
		em.getTransaction().begin();
		return (em);
	}

	public void closeEntityManager(EntityManager em) {
		if (em != null) {
			if (em.isOpen()) {
				EntityTransaction t = em.getTransaction();
				if (t.isActive()) {
					try {
						t.rollback();
					} catch (PersistenceException e) {
						e.printStackTrace();
					}
				}
				em.close();
			}
		}
	}

	public T find(int id) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			return em.find(this.clazz, id);
		} finally {
//			monLogger.info("Entité trouvée");
			closeEntityManager(em);
		}

	}

	public Collection<T> findAllNamedQuery(String query) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<T> q = em.createNamedQuery(query, this.clazz);
			return q.getResultList();
		} finally {
//			monLogger.info("Entité listée");
			closeEntityManager(em);
		}
	}

	public T add(T entity) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			em.persist(entity);
			em.getTransaction().commit();
			return (entity);
		} finally {
//			monLogger.info("Entité ajoutée");
			closeEntityManager(em);
		}
	}

	public T update(T entity) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			entity = em.merge(entity);
			em.getTransaction().commit();
		} finally {
//			monLogger.info("Entité mise à jour");
			closeEntityManager(em);
		}
		return entity;
	}

	public void remove(int id) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			T entity = em.find(this.clazz, id);
			if (entity != null) {
				em.remove(entity);
			}
			em.getTransaction().commit();
		} finally {
//			monLogger.info("Entité supprimée");
			closeEntityManager(em);
		}

	}

}
