package com.afpa.dao;

import java.util.List;

import com.afpa.entity.Couleur;
import com.afpa.entity.Marque;
import com.afpa.entity.Voiture;

/**
 * <b>IVoiture est la classe contenant la déclaration de deux méthodes.
 * Les classes implémentant IVoiture devront obligatoirement implémenter ses méthodes.</b>
 * <p>
 * Les classes à implémenter sont les suisvantes : 
 * <ul>
 * <li>Récupérer une liste de voiture pas une couleur</li>
 * <li>Récupérer la liste de voiture la plus muissante de chaque marque</li>
 * </ul>
 * </p>
 * 
 * @author Amaia, Badrane, Linda
 * 
 *@version 1.0
 */
public interface IVoiture {

	public List<Voiture> listerVoitureByCouleur(Couleur couleur);
	public List<Voiture> listerVoiturePuissanteByMarque();
}
